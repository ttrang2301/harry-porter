import static org.junit.Assert.assertTrue;

import java.util.Objects;

import org.junit.After;
import org.junit.Test;

import ttrang.sample.unittest.Cart;
import ttrang.sample.unittest.Cashier;

public class CashierTest {

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMinPriceOfComboSpecial() {
		Cashier cashier = new Cashier();
		assertTrue(Objects.equals(51.2f, cashier.minPrice(new Cart(2, 2, 2, 1, 1))));
	}

	@Test
	public void testMinPriceOfCombo5() {

		Cashier cashier = new Cashier();

		// Combo 5
		assertTrue(Objects.equals(30f, cashier.minPrice(new Cart(1, 1, 1, 1, 1))));
	}

	@Test
	public void testMinPriceOfCombo4() {

		Cashier cashier = new Cashier();

		// Combo 4
		assertTrue(Objects.equals(25.6f, cashier.minPrice(new Cart(1, 1, 1, 1, 0))));
		assertTrue(Objects.equals(25.6f, cashier.minPrice(new Cart(1, 1, 1, 0, 1))));
		assertTrue(Objects.equals(25.6f, cashier.minPrice(new Cart(1, 1, 0, 1, 1))));
		assertTrue(Objects.equals(25.6f, cashier.minPrice(new Cart(1, 0, 1, 1, 1))));
		assertTrue(Objects.equals(25.6f, cashier.minPrice(new Cart(0, 1, 1, 1, 1))));
	}

	@Test
	public void testMinPriceOfCombo3() {

		Cashier cashier = new Cashier();

		// Combo 3
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 1, 1, 0, 0))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 1, 0, 1, 0))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 0, 1, 1, 0))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(0, 1, 1, 1, 0))));

		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 1, 0, 0, 1))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 0, 1, 0, 1))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(0, 1, 1, 0, 1))));

		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 0, 0, 1, 1))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(0, 1, 0, 1, 1))));

		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(0, 0, 1, 1, 1))));

	}

	@Test
	public void testMinPriceOfCombo1() {

		Cashier cashier = new Cashier();

		// Combo 1
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 1, 1, 0, 0))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 1, 0, 1, 0))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 0, 1, 1, 0))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(0, 1, 1, 1, 0))));

		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 1, 0, 0, 1))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 0, 1, 0, 1))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(0, 1, 1, 0, 1))));

		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(1, 0, 0, 1, 1))));
		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(0, 1, 0, 1, 1))));

		assertTrue(Objects.equals(21.6f, cashier.minPrice(new Cart(0, 0, 1, 1, 1))));

	}

}
