package ttrang.sample.unittest;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Cart {

	public int numberCopiesOfVolume1;
	public int numberCopiesOfVolume2;
	public int numberCopiesOfVolume3;
	public int numberCopiesOfVolume4;
	public int numberCopiesOfVolume5;

	public Cart(int numberCopiesOfVolume1, int numberCopiesOfVolume2, int numberCopiesOfVolume3, int numberCopiesOfVolume4,
			int numberCopiesOfVolume5) {
		super();
		this.numberCopiesOfVolume1 = numberCopiesOfVolume1;
		this.numberCopiesOfVolume2 = numberCopiesOfVolume2;
		this.numberCopiesOfVolume3 = numberCopiesOfVolume3;
		this.numberCopiesOfVolume4 = numberCopiesOfVolume4;
		this.numberCopiesOfVolume5 = numberCopiesOfVolume5;
	}

	public boolean isEmpty() {
		return this.numberCopiesOfVolume1 == 0 && this.numberCopiesOfVolume2 == 0 && this.numberCopiesOfVolume3 == 0
				&& this.numberCopiesOfVolume4 == 0 && this.numberCopiesOfVolume5 == 0;
	}

	public int numberOfVolumes() {
		return (this.numberCopiesOfVolume1 > 0 ? 1 : 0) + (this.numberCopiesOfVolume2 > 0 ? 1 : 0)
				+ (this.numberCopiesOfVolume3 > 0 ? 1 : 0) + (this.numberCopiesOfVolume4 > 0 ? 1 : 0)
				+ (this.numberCopiesOfVolume5 > 0 ? 1 : 0);
	}

	public int biggestAvailableCombo() {
		return this.numberOfVolumes();
	}

	private static final class ProductQuantity {
		public int volumeNumber;
		public int quantity;

		public ProductQuantity(int volumeNumber, int quantity) {
			super();
			this.volumeNumber = volumeNumber;
			this.quantity = quantity;
		}
	}

	public Cart afterDiscountOnNumberOfVolumes(int numberOfDiscountedVolumes) {

		Cart restOfCombo = new Cart(this.numberCopiesOfVolume1, this.numberCopiesOfVolume2, this.numberCopiesOfVolume3,
				this.numberCopiesOfVolume4, this.numberCopiesOfVolume5);

		List<ProductQuantity> list = Arrays.asList(new ProductQuantity(1, this.numberCopiesOfVolume1),
				new ProductQuantity(2, this.numberCopiesOfVolume2), new ProductQuantity(3, this.numberCopiesOfVolume3),
				new ProductQuantity(4, this.numberCopiesOfVolume4), new ProductQuantity(5, this.numberCopiesOfVolume5));
		List<Object> sortedList = list.stream().sorted(new Comparator<ProductQuantity>() {

			public int compare(ProductQuantity o1, ProductQuantity o2) {
				return -Integer.compare(o1.quantity, o2.quantity);
			}
		}).collect(Collectors.toList());

		for (int i = 0; i < numberOfDiscountedVolumes; i++) {
			ProductQuantity productQuantity = (ProductQuantity) sortedList.get(i);
			switch (productQuantity.volumeNumber) {
			case 1:
				restOfCombo.numberCopiesOfVolume1--;
				break;
			case 2:
				restOfCombo.numberCopiesOfVolume2--;
				break;
			case 3:
				restOfCombo.numberCopiesOfVolume3--;
				break;
			case 4:
				restOfCombo.numberCopiesOfVolume4--;
				break;
			case 5:
				restOfCombo.numberCopiesOfVolume5--;
				break;
			default:
				break;
			}
		}

		return restOfCombo;
	}

	@Override
	public String toString() {
		return "Cart [" + numberCopiesOfVolume1 + ", " + numberCopiesOfVolume2 + ", " + numberCopiesOfVolume3 + ", " + numberCopiesOfVolume4
				+ ", " + numberCopiesOfVolume5 + "]";
	}
}
