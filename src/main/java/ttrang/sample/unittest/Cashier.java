package ttrang.sample.unittest;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Cashier {

	/**
	 * 
	 * @param cart
	 * @return min price of cart
	 */
	public float minPrice(Cart cart) {
		if (cart.isEmpty()) {
			return 0;
		}
		int biggestAvailableCombo = cart.biggestAvailableCombo();
		List<Float> availablePrices = new ArrayList<Float>(biggestAvailableCombo);
		for (int appliedCombo = biggestAvailableCombo; appliedCombo > 0; appliedCombo--) {
			Cart restCart = cart.afterDiscountOnNumberOfVolumes(appliedCombo);
			float cartPrice = Combo.getPrice(appliedCombo) + minPrice(restCart);
			availablePrices.add(cartPrice);
		}
		Float minPrice = getMinValueFromList(availablePrices);
		return minPrice;
	}

	private Float getMinValueFromList(List<Float> availablePrices) {
		return availablePrices.stream().min(new Comparator<Float>() {
			public int compare(Float o1, Float o2) {
				return o1.compareTo(o2);
			}
		}).get();
	}

	public static void main(String[] args) {
		Cashier cashier = new Cashier();
		System.out.println("Last decision: " + cashier.minPrice(new Cart(3, 3, 2, 2, 2)));
	}
}
