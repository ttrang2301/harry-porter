package ttrang.sample.unittest;

public enum Combo {
	COMBO_1(8), COMBO_2(15.2f), COMBO_3(21.6f), COMBO_4(25.6f), COMBO_5(30);

	private float price;

	private Combo(float price) {
		this.price = price;
	}

	public float getPrice() {
		return price;
	}

	public static final float getPrice(int numberOfVolumes) {
		if (numberOfVolumes < 0 || numberOfVolumes > Combo.values().length) {
			throw new RuntimeException(
					"Invalid numberOfVolumes: " + numberOfVolumes + " .Can only be 0-" + Combo.values().length);
		}
		if (numberOfVolumes == 0) {
			return 0;
		}
		for (int index = 0; index < Combo.values().length; index++) {
			if (numberOfVolumes == (index + 1)) {
				return Combo.values()[index].getPrice();
			}
		}
		throw new RuntimeException(
				"Unexpected exception calculating price for combo of : " + numberOfVolumes + " volumes.");
	}
}
